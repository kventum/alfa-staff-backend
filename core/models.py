from django.db import models
from django.contrib.auth.models import AbstractUser


class Resource(models.Model):
    title = models.CharField(max_length=60, blank=False)
    url = models.CharField(max_length=60, blank=False)

    def __str__(self):
        return '%s: %s' % (self.title, self.url)

    class Meta:
        verbose_name = "Ресурс"
        verbose_name_plural = "Ресурсы"


class Position(models.Model):
    title = models.CharField(max_length=60, blank=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Должность"
        verbose_name_plural = "Должности"


class User(AbstractUser):
    ACTIVE = 'active'
    FIRED = 'fired'
    ON_VACATION = 'on-vacation'
    ON_A_SICK_LEAVE = 'on-a-sick-leave'
    STATUS_CHOICES = [
        (ACTIVE, 'работает'),
        (FIRED, 'уволен'),
        (ON_VACATION, 'в отпуске'),
        (ON_A_SICK_LEAVE, 'на больничном'),
    ]

    status = models.CharField(
        max_length=30,
        choices=STATUS_CHOICES,
        default=ACTIVE,
    )

    first_name = models.CharField(max_length=60, blank=False)
    last_name = models.CharField(max_length=60, blank=False)
    patronymic_name = models.CharField(max_length=60, blank=True)

    email = models.EmailField(max_length=30, unique=True)
    phone = models.CharField(max_length=30, blank=False)

    birth_date = models.DateField(null=True, blank=False)

    inn = models.CharField(max_length=12, blank=True)

    position = models.ForeignKey(Position, null=True, on_delete=models.SET_NULL, blank=True)

    resources = models.ManyToManyField(Resource, blank=True)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"






