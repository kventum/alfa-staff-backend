from django.urls import include, path
# from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from core.views import (
    registration_view,
    users_view,
    positions_view,
    resources_view,
    user_detail_view,
    position_detail_view,
    resource_detail_view
)

app_name = 'core'

urlpatterns = [
    path('auth-token', TokenObtainPairView.as_view()),
    path('auth-token-refresh', TokenRefreshView.as_view()),

    path('register', registration_view, name="register"),
    path('users', users_view, name="users"),
    path('users/<int:pk>', user_detail_view, name="user"),
    path('positions', positions_view, name="positions"),
    path('positions/<int:pk>', position_detail_view, name="position"),
    path('resources', resources_view, name="resources"),
    path('resources/<int:pk>', resource_detail_view, name="resource"),
]
