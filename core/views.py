from django.db.models import Count

from rest_framework import status
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.decorators import api_view


from core.serializers import (
    RegistrationSerializer,
    UserSerializer,
    PositionSerializer,
    ResourceSerializer
)


from core.models import (
    User,
    Resource,
    Position
)


@api_view(['POST', ])
def registration_view(request):
    if request.method == 'POST':
        serializer = RegistrationSerializer(data=request.data)

        data = {}

        if serializer.is_valid():
            user = serializer.save()
            data = user.id
            # Отправить пароль на почту
            # user.email
            # user.inn
        else:
            data = serializer.errors

        return Response(data)


@api_view(['GET', ], )
def users_view(request):
    if request.method == 'GET':
        users = User.objects.exclude(id=1)
        serializer = UserSerializer(users, many=True)

        return Response(serializer.data)

@api_view(['GET', ], )
def user_detail_view(request, pk):
    if request.method == 'GET':
        if pk == 1:
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            user = User.objects.get(pk=pk)
            serializer = UserSerializer(user)

            return Response(serializer.data)
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'POST', ])
def positions_view(request):
    if request.method == 'GET':
        positions = Position.objects.all().annotate(
            users_count=Count('user')
        )
        serializer = PositionSerializer(positions, many=True)

        return Response(serializer.data)
    if request.method == 'POST':
        serializer = PositionSerializer(data=request.data)

        data = {}

        if serializer.is_valid():
            position = serializer.save()
            data = position.id
        else:
            data = serializer.errors

        return Response(data)

@api_view(['GET', ], )
def position_detail_view(request, pk):
    if request.method == 'GET':
        try:
            position = Position.objects.get(pk=pk)
            positionSerializer = PositionSerializer(position)

            users = User.objects.filter(position=pk)
            usersSerializer = UserSerializer(users, many=True)

            data = {
                'position': positionSerializer.data,
                'users': usersSerializer.data
            }

            return Response(data)
        except Position.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'POST', ])
def resources_view(request):
    if request.method == 'GET':
        resources = Resource.objects.all()
        serializer = ResourceSerializer(resources, many=True)

        return Response(serializer.data)
    if request.method == 'POST':
        serializer = ResourceSerializer(data=request.data)

        data = {}

        if serializer.is_valid():
            resource = serializer.save()
            data = resource.id
        else:
            data = serializer.errors

        return Response(data)

@api_view(['GET', ], )
def resource_detail_view(request, pk):
    if request.method == 'GET':
        try:
            resource = Resource.objects.get(pk=pk)
            serializer = ResourceSerializer(resource)

            return Response(serializer.data)
        except Resource.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)