from django.contrib import admin

# Register your models here.
from core.models import User, Position, Resource

class UserAdmin(admin.ModelAdmin):
    pass

class PositionAdmin(admin.ModelAdmin):
    pass

class ResourceAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)
admin.site.register(Position, PositionAdmin)
admin.site.register(Resource, ResourceAdmin)