from rest_framework import serializers


from core.models import (User, Resource, Position)


class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = "__all__"

    def save(self):
        resource = Resource(
            title = self.validated_data['title'],
            url = self.validated_data['url'],
        )
        resource.save()

        return resource

class PositionSerializer(serializers.ModelSerializer):
    users_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Position
        fields = [
            'id',
            'title',
            'users_count'
        ]

    def save(self):
        position = Position(
            title = self.validated_data['title'],
        )
        position.save()

        return position

class UserSerializer(serializers.ModelSerializer):
    resources = ResourceSerializer(many=True)
    position = PositionSerializer()

    class Meta:
        model = User
        fields = [
            'id',
            'status',
            'first_name',
            'last_name',
            'patronymic_name',
            'email',
            'phone',
            'birth_date',
            'inn',
            'position',
            'resources',
        ]

class RegistrationSerializer(serializers.ModelSerializer):
    resources = serializers.PrimaryKeyRelatedField(queryset=Resource.objects.all(), many=True)

    class Meta:
        model = User
        fields = [
            'status',
            'first_name',
            'last_name',
            'patronymic_name',
            'email',
            'phone',
            'birth_date',
            'inn',
            'position',
            'resources',
        ]

    def save(self):
        resources = self.validated_data.pop('resources', [])

        user = User(
            username = self.validated_data['inn'],
            status = self.validated_data['status'],
            first_name = self.validated_data['first_name'],
            last_name = self.validated_data['last_name'],
            patronymic_name = self.validated_data['patronymic_name'],
            email = self.validated_data['email'],
            phone = self.validated_data['phone'],
            birth_date = self.validated_data['birth_date'],
            inn = self.validated_data['inn'],
            position=self.validated_data['position'],
        )

        user.set_password(user.inn)
        user.save()

        for resource in resources:
            user.resources.add(resource)

        return user