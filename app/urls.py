from django.urls import include, path
from django.contrib import admin
from rest_framework import routers

router = routers.DefaultRouter()

urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/', include('core.urls', 'core_api')),
]
